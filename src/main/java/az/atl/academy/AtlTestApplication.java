package az.atl.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtlTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AtlTestApplication.class, args);
    }

}
