package az.atl.academy.service.impl;

import az.atl.academy.dao.repository.StudentRepository;
import az.atl.academy.model.dto.StudentDto;
import az.atl.academy.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

import static az.atl.academy.model.builder.StudentBuilder.STUDENT_BUILDER;
import static az.atl.academy.model.consts.OperationMessages.GREETINGS;
import static az.atl.academy.model.consts.OperationMessages.USER_NOT_FOUND;

@Service
@AllArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final MessageSource messageSource;
    private final StudentRepository studentRepository;

    @Override
    public void create(StudentDto studentDto) {
        Locale locale = LocaleContextHolder.getLocale();
        log.info("Locale: " + locale);
        Object[] obj = new Object[2];
        obj[0] = studentDto.getFullName();
        obj[1] = studentDto.getId();
        String message = messageSource.getMessage(GREETINGS.getMessage(), obj, locale);
        log.info(message);

        var studentEntity = STUDENT_BUILDER.buildEntity(studentDto);
        studentRepository.save(studentEntity);

        log.info("Student created " + studentDto);
    }

    @Override
    public StudentDto getById(Long id) {
        Locale locale = LocaleContextHolder.getLocale();
        var entity = studentRepository.findById(id);
        if (entity.isPresent())
            return STUDENT_BUILDER.buildDto(entity.get());
        Object[] obj = new Object[1];
        obj[0] = id;
        throw new RuntimeException(messageSource.getMessage(USER_NOT_FOUND.getMessage(), obj, locale));
    }

    @Override
    public List<StudentDto> getAll() {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }
}
