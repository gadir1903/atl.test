package az.atl.academy.service;

import az.atl.academy.model.dto.StudentDto;

import java.util.List;

public interface StudentService {
    void create(StudentDto studentDto);

    StudentDto getById(Long id);

    List<StudentDto> getAll();

    void deleteById(Long id);
}
