package az.atl.academy.annotation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<PhoneValidation, String> {
    @Override
    public void initialize(PhoneValidation phoneValidation) {
    }

    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext context) {
        return contactField.length() == 13 && contactField.startsWith("+994");
    }
}
