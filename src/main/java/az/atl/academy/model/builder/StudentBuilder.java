package az.atl.academy.model.builder;

import az.atl.academy.dao.entity.StudentEntity;
import az.atl.academy.model.dto.StudentDto;

public enum StudentBuilder {
    STUDENT_BUILDER;

    public final StudentEntity buildEntity(StudentDto studentDto) {
        return StudentEntity.builder()
                .id(studentDto.getId())
                .studentId(studentDto.getStudentId())
                .fullName(studentDto.getFullName())
                .email(studentDto.getEmail())
                .phoneNumber(studentDto.getPhoneNumber())
                .age(studentDto.getAge())
                .build();
    }

    public final StudentDto buildDto(StudentEntity entity) {
        return StudentDto.builder()
                .id(entity.getId())
                .studentId(entity.getStudentId())
                .fullName(entity.getFullName())
                .phoneNumber(entity.getPhoneNumber())
                .age(entity.getAge())
                .build();
    }
}
