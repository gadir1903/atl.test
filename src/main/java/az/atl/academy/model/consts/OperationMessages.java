package az.atl.academy.model.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OperationMessages {
    GREETINGS("greetings"), VALID_FULL_NAME("valid.fullName"), VALID_PHONENUMBER("phoneNumber.notValid") , USER_NOT_FOUND("student.notFound") ,;

    private final String message;
}
