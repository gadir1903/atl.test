package az.atl.academy.model.dto;

import az.atl.academy.annotation.PhoneValidation;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class StudentDto {
    Long id;
    String studentId;
    @NotNull(message = "{valid.fullName}")
    String fullName;
    Integer age;
    @PhoneValidation(message = "{phoneNumber.notValid}")
    String phoneNumber;
    String email;
}
